module gitee.com/ks-custle/sarama-gm

go 1.17

require (
	gitee.com/ks-custle/core-gm v0.0.0-20230922171213-b83bdd97b62c
	github.com/DataDog/zstd v1.5.0
	github.com/davecgh/go-spew v1.1.1
	github.com/eapache/go-resiliency v1.2.0
	github.com/eapache/go-xerial-snappy v0.0.0-20180814174437-776d5712da21
	github.com/eapache/queue v1.1.0
	github.com/pierrec/lz4 v2.6.1+incompatible
	github.com/rcrowley/go-metrics v0.0.0-20201227073835-cf1acfcdf475
)

require (
	github.com/frankban/quicktest v1.14.2 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	golang.org/x/crypto v0.1.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
)

// replace gitee.com/ks-custle/core-gm => /home/go-workspace/src/github.com/hyperledger/core-gm
